package org.changken.tutorialsample;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class NoticeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);

        Button noticeButton = (Button) findViewById(R.id.notice_button);

        noticeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    //設定頻道
                    NotificationChannel channel = new NotificationChannel("Tutor", "TutorialSample", NotificationManager.IMPORTANCE_HIGH);
                    channel.setDescription("教學用的頻道");
                    channel.enableLights(true);
                    channel.enableVibration(true);

                    //加入頻道
                    NotificationManager manager = getSystemService(NotificationManager.class);
                    manager.createNotificationChannel(channel);

                    //建構一則通知
                    Notification.Builder builder = new Notification.Builder(NoticeActivity.this, "Tutor")
                            .setSmallIcon(R.drawable.icons8_android_64)
                            .setContentTitle("你好")
                            .setContentText("c8763!")
                            .setChannelId("Tutor");

                    //顯示他
                    manager.notify(1, builder.build());
                } else {
                    //通知管理器
                    NotificationManagerCompat managerCompat = NotificationManagerCompat.from(NoticeActivity.this);

                    //建構一則通知
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(NoticeActivity.this)
                            .setSmallIcon(R.drawable.icons8_android_64)
                            .setContentTitle("你好")
                            .setContentText("c8763!")
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                    //發送通知
                    managerCompat.notify(1, builder.build());
                }
            }
        });
    }
}
