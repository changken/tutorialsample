package org.changken.tutorialsample;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainFragment extends Fragment {

    private View mainView;
    private TextView mainMessage;
    private String showMessage = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_main, container, false);
        mainMessage = (TextView) mainView.findViewById(R.id.main_message);
        mainMessage.setText(showMessage);
        return mainView;
    }

    /**
     * 設定mainMessage的文字內容
     * */
    public void setMainMessage(String message){
        Log.v("MainFragment", message);
        /*
        因為根據fragment的生命週期，只要使用者使用nav返回到MainFragment時，它會再次呼叫 onCreateView!
        * */
        showMessage = message;
    }
}
