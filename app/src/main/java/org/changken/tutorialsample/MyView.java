package org.changken.tutorialsample;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

public class MyView extends View {
    public MyView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        //自訂背景顏色為綠色
        setBackgroundColor(Color.GREEN);
    }

    /**
     * 自動會呼叫
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float ex = event.getX(), ey = event.getY();
        Log.v("MyView", "x = " + ex + ",y = " + ey);
        super.onTouchEvent(event);
        return true;
    }
}
