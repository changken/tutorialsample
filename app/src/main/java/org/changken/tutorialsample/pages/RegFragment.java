package org.changken.tutorialsample.pages;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.changken.tutorialsample.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_page, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.msg);
        textView.setText("Register");
        return rootView;
    }

}
