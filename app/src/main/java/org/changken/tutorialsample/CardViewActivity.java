package org.changken.tutorialsample;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

public class CardViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view);

        LinkedList<Post> list = new LinkedList<>();

        list.add(new Post("JJ", "小情割"));
        list.add(new Post("我好", "乾杯"));
        list.add(new Post("你不好", "七月天"));
        list.add(new Post("屋頂", "中縣屋"));
        list.add(new Post("彩色氣球", "要開了"));
        list.add(new Post("夢幻氣球", "哈哈"));
        list.add(new Post("告白氣球", "派對動誤"));
        list.add(new Post("聽下雨的聲音", "週姓董事"));
        list.add(new Post("周董", "不垓"));
        list.add(new Post("張學友", "冷冷的冰宇"));

        RecyclerView listCardView = (RecyclerView) findViewById(R.id.list_card_view);

        listCardView.setLayoutManager(new LinearLayoutManager(this));

        CardAdapter cardAdapter = new CardAdapter(this, list);

        cardAdapter.setMyOnItemClickListener(new MyOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Toast.makeText(CardViewActivity.this, "項目 " + position, Toast.LENGTH_SHORT).show();
            }
        });

        listCardView.setAdapter(cardAdapter);
    }

    private interface MyOnItemClickListener {
        void onItemClick(View view, int position);
    }

    private class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {
        private Context mContext;
        private List<Post> mList;
        private MyOnItemClickListener mMyOnItemClickListener;

        public CardAdapter(Context context, List<Post> list) {
            mContext = context;
            mList = list;
        }

        public void setMyOnItemClickListener(MyOnItemClickListener myOnItemClickListener) {
            mMyOnItemClickListener = myOnItemClickListener;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater
                    .from(mContext)
                    .inflate(R.layout.card_item, parent, false);
            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
            viewHolder.titleTextView.setText(mList.get(position).getTitle());
            viewHolder.contentTextView.setText(mList.get(position).getContent());
            viewHolder.deleteButton.setText("Delete");
            viewHolder.bind(viewHolder.itemView, position);
        }

        @Override
        public int getItemCount() {
            return mList.size();
        }

        private class ViewHolder extends RecyclerView.ViewHolder {
            private TextView titleTextView;
            private TextView contentTextView;
            private Button deleteButton;

            public ViewHolder(View listItem) {
                super(listItem);

                titleTextView = (TextView) listItem.findViewById(R.id.title_text_view);
                contentTextView = (TextView) listItem.findViewById(R.id.content_text_view);
                deleteButton = (Button) listItem.findViewById(R.id.delete_button);
            }

            public void bind(final View view, final int position) {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mMyOnItemClickListener != null) {
                            mMyOnItemClickListener.onItemClick(v, position);
                        }else{
                            Log.e("CardAdapter", "[Error] 請設定MyOnItemClickListener!");
                        }
                    }
                });
            }
        }
    }
}
