package org.changken.tutorialsample;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {

    private View mainView;
    private TestFragmentActivity testFragmentActivity;

    /**
     * Fragment的生命週期 onAttach()
     * */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*
        儲存TestFragmentActivity物件
        因為fragment之間彼此都不認識，但他們彼此都認識TestFragmentActivity唷!
        */
        //因為Activity本身是繼承Context抽象類別而來的
        testFragmentActivity = (TestFragmentActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_second, container, false);
        Button secondBtn = (Button) mainView.findViewById(R.id.second_btn);
        secondBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                test(v);
            }
        });
        return mainView;
    }

    /**
     * secondBtn的觸發邏輯
     */
    private void test(View v){
        Log.v("SecondFragment", "喔!");
        /*
        * 透過TestFragmentActivity來認識MainFragment
        * 設定MainFragment的MainMessage的文字內容
        * */
        testFragmentActivity.getMainFragment().setMainMessage("Lottery: " + (int)(Math.random() * 49 + 1));
    }

}
