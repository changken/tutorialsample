package org.changken.tutorialsample;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class TestFragmentActivity extends AppCompatActivity {

    private MainFragment mainFragment;
    private SecondFragment secondFragment;
    private ThirdFragment thirdFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_fragment);

        Button mainFragmentButton = (Button) findViewById(R.id.main_fragment_button);
        Button secondFragmentButton = (Button) findViewById(R.id.second_fragment_button);
        Button thirdFragmentButton = (Button) findViewById(R.id.third_fragment_button);

        //初始化Fragment
        mainFragment = new MainFragment();
        secondFragment = new SecondFragment();
        thirdFragment = new ThirdFragment();

        //得到Support FragmentManager
        final FragmentManager fragmentManager = getSupportFragmentManager();

        //開始Fragment交易
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        //預設把MainFragment給放上舞台
        fragmentTransaction.add(R.id.fragment_container, mainFragment);
        //提交
        fragmentTransaction.commit();

        mainFragmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //開始Fragment交易
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //把MainFragment給放上舞台
                fragmentTransaction.replace(R.id.fragment_container, mainFragment);
                //提交
                fragmentTransaction.commit();
            }
        });

        secondFragmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //開始Fragment交易
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //把MainFragment給放上舞台
                fragmentTransaction.replace(R.id.fragment_container, secondFragment);
                //提交
                fragmentTransaction.commit();
            }
        });

        thirdFragmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //開始Fragment交易
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //把MainFragment給放上舞台
                fragmentTransaction.replace(R.id.fragment_container, thirdFragment);
                //提交
                fragmentTransaction.commit();
            }
        });
    }

    /**
     * 取得MainFragment物件
     * */
    public MainFragment getMainFragment(){
        return mainFragment;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fragment, menu);
        return true;
    }
}
