package org.changken.tutorialsample;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    String test = "attribute";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        //1. 取得ListView
        ListView listView = (ListView) findViewById(R.id.list_view);

        //2. 建立ArrayList
        final ArrayList<String> list = new ArrayList<>();

        //2.1. 放資料
        for (int i = 0; i < 20; i++) {
            list.add("你好 " + i);
        }

        //建立自定義Adapter
        MyAdapter arrayAdapter = new MyAdapter(
                this, //傳入ListActivity的目前狀態
                list //List資料
        );

        //設定項目點擊監聽器
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListActivity.this, "項目點擊事件 " + test + " " + list.get(position), Toast.LENGTH_SHORT).show();
            }
        });

        //設定Adapter
        listView.setAdapter(arrayAdapter);
    }

    class MyAdapter extends ArrayAdapter<String> {
        private Context mContext;//儲存ListActivity的目前狀態
        private ArrayList<String> mList; //List資料

        public MyAdapter(Context context, ArrayList<String> list) {
            //使用父類別的建構子方法
            super(context, 0, list);

            //儲存相關資料
            mContext = context;
            mList = list;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View listview = convertView;

            //如果資料不存在的話，新增一筆資料
            if (listview == null) {
                listview = LayoutInflater
                        .from(mContext)//傳入Activity的狀態
                        .inflate(R.layout.list_item, //自定義的列表外觀
                                parent,//父元件
                                false);//不要把父元件加進來
            }

            //設定資料
            TextView textView = (TextView) listview.findViewById(R.id.text);
            textView.setText(mList.get(position));

            Button button = (Button) listview.findViewById(R.id.btn);
            button.setText("delete");
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "按鈕點擊事件 " + mList.get(position), Toast.LENGTH_SHORT).show();
                }
            });

            //回傳此筆View資料
            return listview;
        }
    }
}
