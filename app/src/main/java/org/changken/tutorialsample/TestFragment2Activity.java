package org.changken.tutorialsample;

import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.changken.tutorialsample.pages.IntroFragment;
import org.changken.tutorialsample.pages.RegFragment;

import java.util.ArrayList;
import java.util.List;

public class TestFragment2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_fragment2);

        List<FragmentItem> list = new ArrayList<>();
        list.add(new FragmentItem("介紹", new IntroFragment()));
        list.add(new FragmentItem("註冊", new RegFragment()));

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager(), list));

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {
        private List<FragmentItem> mList;

        public MyFragmentPagerAdapter(FragmentManager fm, List<FragmentItem> list) {
            super(fm);
            mList = list;
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public Fragment getItem(int i) {
            return mList.get(i).getFragment();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mList.get(position).getTitle();
        }
    }

    private class FragmentItem{
        private String mTitle;
        private Fragment mFragment;

        public FragmentItem(String title, Fragment fragment) {
            mTitle = title;
            mFragment = fragment;
        }

        public String getTitle() {
            return mTitle;
        }

        public void setTitle(String title) {
            mTitle = title;
        }

        public Fragment getFragment() {
            return mFragment;
        }

        public void setFragment(Fragment fragment) {
            mFragment = fragment;
        }
    }

}
