package org.changken.tutorialsample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

//繼承
public class TestActivity extends AppCompatActivity {

    //覆寫
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        Button clickMeButton = (Button) findViewById(R.id.click_me_button);

        //EditText物件
        final EditText editText = (EditText) findViewById(R.id.editText);

        clickMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent(所在的頁面, 要去的頁面);
                // Context => 頁面目前的狀態
                Intent intent = new Intent(TestActivity.this, Main2Activity.class);
                //傳資料
                //類似耀輝教的
                intent.putExtra("name", editText.getText());
                //啟動intent
                startActivity(intent);
            }
        });

        Button listButton = (Button) findViewById(R.id.list_button);

        listButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestActivity.this, ListActivity.class);
                startActivity(intent);
            }
        });

        Spinner spinner = (Spinner) findViewById(R.id.test_spinner);

        List<String> list = new ArrayList<>();
        list.add("俊昇");
        list.add("名騎");
        list.add("童郁");
        list.add("凱凱");
        list.add("木桁");
        list.add("品均");
        list.add("紫薇");
        list.add("大為");
        list.add("翔鈴");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                list);

        spinner.setAdapter(arrayAdapter);
    }
}
