package org.changken.tutorialsample;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

public class Nav {
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private Toolbar mToolbar;

    private Context mContext;
    private AppCompatActivity mActivity;

    public Nav(DrawerLayout drawerLayout, NavigationView navigationView, Toolbar toolbar, AppCompatActivity activity) {
        mDrawerLayout = drawerLayout;
        mNavigationView = navigationView;
        mToolbar = toolbar;

        //取得Activity
        mActivity = activity;
        //取得context
        mContext = (Context) activity;
    }

    public void setNav(){
        //設置Toolbar
        mActivity.setSupportActionBar(mToolbar);

        //為navigatin_view設置點擊事件
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                // 點選時收起選單
                mDrawerLayout.closeDrawer(GravityCompat.START);
                // 取得選項id
                int id = item.getItemId();
                // 依照id判斷點了哪個項目並做相應事件
                if (id == R.id.action_home) {
                    // 按下「首頁」要做的事
                    Toast.makeText(mContext, "首頁", Toast.LENGTH_SHORT).show();
                    return true;
                } else if (id == R.id.action_help) {
                    // 按下「使用說明」要做的事
                    Toast.makeText(mContext, "使用說明", Toast.LENGTH_SHORT).show();
                    return true;
                } else if (id == R.id.action_go) {
                    Toast.makeText(mContext, "去吧", Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;
            }
        });

        //將drawerLayout和toolbar整合，會出現「三」按鈕
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                mActivity, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawerLayout.addDrawerListener(toggle);

        toggle.syncState();
    }
}
